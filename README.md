### Install softether latest on ubuntu 20.xx fresh install 



###### automatic install:

```bash
wget -O- https://bitbucket.org/serkanp/softethetubuntu20/raw/master/setup.sh | bash
```



###### manuel install:

```bash
cd ~/

wget  https://bitbucket.org/serkanp/softethetubuntu20/raw/master/setup.sh 

chmod +x setup.sh

./setup.sh
```



###### Written by :

[Serkan Polat]: serkan@smg.com.tr

###### Test, help, ideas, recommendations:

[Atıf Zafrak]: atif@zafrak.com
[Coskun Sunalı]: coskun@codinq.com

