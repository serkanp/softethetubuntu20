#!/bin/bash
SERVER_PASS="1q2w3e4R"
NEWHOST="serkanvpn6"
EMAIL="serkan@smg.com.tr"
HUB="VPN"
cmdpath=/usr/local/vpnserver/vpncmd
sethostresult="0"
function sethostname()
{
	sethostresult="0"
	checkhost
	if [[ "$hostname" == "$NEWHOST" ]]
	then
	  echo "new hostname $NEWHOST is same with the new one.exiting.."
	  sethostresult="1"
	else
		hostcheck=$(/usr/local/vpnserver/vpncmd localhost /SERVER /PASSWORD:${SERVER_PASS} /CMD DynamicDnsSetHostname ${NEWHOST})
		echo "sonuc: $hostcheck"
		if [[ "$hostcheck" == *"The command completed successfully."* ]]
		then
			  clear
		  checkhost
		  if [[ "$hostname" == "$NEWHOST" ]]
		  then 
			  echo "Operation Completed Successfully!"
			  sethostresult="1"
		  else
			 echo "something go wrong"
		  fi
		else
		  clear
		  echo "Operation NOT COMPLETED"
		fi
	fi

}


function checkhost()
{

fullhost=$(${cmdpath} localhost /SERVER /PASSWORD:${SERVER_PASS} /CMD DynamicDnsGetStatus |grep -n "Assigned Dynamic DNS Hostname (Full)"|rev|cut -d"|" -f1|rev)

hostname=$(${cmdpath} localhost /SERVER /PASSWORD:${SERVER_PASS} /CMD DynamicDnsGetStatus |grep -n "Assigned Dynamic DNS Hostname (Hostname)"|rev|cut -d"|" -f1|rev)

}

function installcertbot()
{
	if [[ "$(which snap)" == "/usr/bin/snap" ]]
	then 
		echo "snap already installed"
	else
	  echo "snap not installed"
		apt install snapd -y 

		#snapd core ve refresh kur
		snap install core; snap refresh core
	fi

	if [[ "$(which certbot)" == "/snap/bin/certbot" ]]
	then 
		echo "certbot already installed"
	else
	  echo "certbot not installed"
		snap install --classic certbot

		ln -s /snap/bin/certbot /usr/bin/certbot
	fi


}


function createssl(){

 sethostname
 checkhost
 if [[ $sethostresult == "1" ]]
 then
	 sleep 3
	 echo "installing certificate"
	 sslhost="${hostname}.softether.net"
	 fullchain="/etc/letsencrypt/live/${hostname}.softether.net/fullchain.pem"
	 privkey="/etc/letsencrypt/live/${hostname}.softether.net/privkey.pem"
	 certbot certonly --standalone --non-interactive --agree-tos -d ${sslhost} -m ${EMAIL}
	 /usr/local/vpnserver/vpncmd /server localhost /password:${SERVER_PASS} /adminhub:${HUB} /CMD ServerCertSet /LOADCERT:${fullchain} /LOADKEY:${privkey}
	 certbot renew --dry-run
	  echo "certificate INSTALLED...."
 else
	echo "certificate NOT INSTALLED"
 fi
}

installcertbot
createssl
